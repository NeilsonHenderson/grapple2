﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vector : MonoBehaviour
{
     Vector3 myVector;
     Rigidbody m_Rigidbody;
    float m_Speed = 2.0f;

    // Start is called before the first frame update
    void Start()
    {
        myVector = new Vector3(0.0f, 1.0f, 0.0f);        

        m_Rigidbody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        m_Rigidbody.velocity = myVector * m_Speed;
    }
}
