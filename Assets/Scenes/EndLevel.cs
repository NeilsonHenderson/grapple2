﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndLevel : MonoBehaviour
{
    private void OnTriggerEnter()
    {
        //activates win message
        GameManager.instance.Win();

    }
}
