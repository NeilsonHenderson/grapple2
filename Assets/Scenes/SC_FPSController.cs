﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Threading;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]


public class SC_FPSController : MonoBehaviour
{
    [SerializeField] private Transform debugHitPointTransform;
    [SerializeField] private Transform hookshotTransform;

    public float walkingSpeed = 7.5f;
    public float runningSpeed = 11.5f;
    public float jumpSpeed = 8.0f;
    public float gravity = 20.0f;
    public Camera playerCamera;
    public float lookSpeed = 2.0f; //mouse sensitivity
    public float lookXLimit = 45.0f; //stops camera from passing a certain point
    private LineRenderer lineRenderer;
    private State state;
    private Vector3 hookshotPosition;
    private Vector3 characterVelocityMomentum;
    private float HookshotSize;


    private enum State
    {
        Normal,
        HookshotFlyingPlayer,
        HookshotThrown
    }

    private void Awake()
    { //turns hookshot invisible while not flinging
        state = State.Normal;
        hookshotTransform.gameObject.SetActive(false);
    }

    CharacterController characterController;
    Vector3 moveDirection = Vector3.zero;
    float rotationX = 2;

    [HideInInspector]
    public bool canMove = true;

    void Start()
    {
        characterController = GetComponent<CharacterController>();

        // Lock cursor
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }


    private void Update()
    {
        switch (state)
        {
            default:
            case State.Normal:
                //HandleHookshotMovement();
                HandleCharacterLook();
                HandleHookShotStart();
                break;
            case State.HookshotThrown:
                HandleHookshotThrow();
                //HandleHookshotMovement();
                HandleCharacterLook();
                break;
            case State.HookshotFlyingPlayer:
                HandleHookshotMovement();
                HandleCharacterLook();
                break;
        }

        //HandleHookShotStart();
        // We are grounded, so recalculate move direction based on axes
        Vector3 forward = transform.TransformDirection(Vector3.forward);
        Vector3 right = transform.TransformDirection(Vector3.right);
        // Press Left Shift to run
        bool isRunning = Input.GetKey(KeyCode.LeftShift);
        float curSpeedX = canMove ? (isRunning ? runningSpeed : walkingSpeed) * Input.GetAxis("Vertical") : 0;
        float curSpeedY = canMove ? (isRunning ? runningSpeed : walkingSpeed) * Input.GetAxis("Horizontal") : 0;
        float movementDirectionY = moveDirection.y;
        moveDirection = (forward * curSpeedX) + (right * curSpeedY);

        if (Input.GetButton("Jump") && canMove && characterController.isGrounded)
        {
            moveDirection.y = jumpSpeed; 
        }
        else
        {
            moveDirection.y = movementDirectionY; 
        }

        // Apply gravity. Gravity is multiplied by deltaTime twice (once here, and once below
        // when the moveDirection is multiplied by deltaTime). This is because gravity should be applied
        // as an acceleration (ms^-2)
        if (!characterController.isGrounded)
        {
            moveDirection.y -= gravity * Time.deltaTime;
        }

        // Move the controller
        characterController.Move(moveDirection * Time.deltaTime);

        // Player and Camera rotation
        if (canMove)
        {
            rotationX += -Input.GetAxis("Mouse Y") * lookSpeed;
            rotationX = Mathf.Clamp(rotationX, -lookXLimit, lookXLimit);
            playerCamera.transform.localRotation = Quaternion.Euler(rotationX, 0, 0);
            transform.rotation *= Quaternion.Euler(0, Input.GetAxis("Mouse X") * lookSpeed, 0);


        }
    }

    private void HandleHookShotStart()
    {
        if (Input.GetKeyDown(KeyCode.E)) //hookshot throws on key press
        {
            if (Physics.Raycast(playerCamera.transform.position, playerCamera.transform.forward, out RaycastHit raycastHit))
            {
                
                //hit something
                debugHitPointTransform.position = raycastHit.point;
                hookshotPosition = raycastHit.point;
                HookshotSize = 0f;
                hookshotTransform.gameObject.SetActive(true);
                hookshotTransform.localScale = Vector3.zero;
                state = State.HookshotThrown;
            }
        }
    }

    private void HandleHookshotThrow()
    {
        hookshotTransform.LookAt(hookshotPosition);

        //Hookshot length when flung
        float HookshotThrowSpeed = 100f;
        HookshotSize += HookshotThrowSpeed * Time.deltaTime;
        hookshotTransform.localScale = new Vector3(1, 1, HookshotSize);

        if (HookshotSize >= Vector3.Distance(transform.position, hookshotPosition))
        {
            state = State.HookshotFlyingPlayer;
        }
    }

    //Locks Hookshot when fired into place, not following camera
    private void HandleHookshotMovement()
    {
        hookshotTransform.LookAt(hookshotPosition);

        Vector3 hookshotDir = (hookshotPosition - transform.position).normalized;

        float hookshotSpeedMin = 30f; //minimum hookshot thrown speed
        float hookshotSpeedMax = 40f; //maximum hookshot thrown speed
        float hookshotSpeed = Mathf.Clamp(Vector3.Distance(transform.position, hookshotPosition), hookshotSpeedMin, hookshotSpeedMax);
        float hookshotSpeedMultiplier = 2f;

        characterController.Move(hookshotDir * hookshotSpeed * hookshotSpeedMultiplier * Time.deltaTime);

        float reachedHookshotPositionDistance = 1f;
        if (Vector3.Distance(transform.position, hookshotPosition) < reachedHookshotPositionDistance)
        {
            state = State.Normal;
            StopHookshot();
        }

        if (TestInputDownHookshot())
        {
            state = State.Normal;
            StopHookshot();
        }

    }

    private bool TestInputDownHookshot()
    {
        return Input.GetKeyDown(KeyCode.E); //press E to hookshot
    }

    private bool TestInputJump() 
    {
        return Input.GetKeyDown(KeyCode.Space); //press space to jump

    }

    private void StopHookshot()
    {
        state = State.Normal;
        hookshotTransform.gameObject.SetActive(false); //turns hookshot invisible when contact with Red is made

    }


    private void HandleCharacterLook() { }

}
